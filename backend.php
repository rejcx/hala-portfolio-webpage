<?
class Storage {
    private static $dataPath = "data/";
    
    private static $pageInfo;
    private static $resume = array();
    private static $portfolio = array();
    
    private static function ReadJson( $path )
    {
        echo( "ReadJson " . $path . "\n" );
        $raw = file_get_contents( $path );
        $array = json_decode( $raw, true );
        $raw = str_replace( "#NAME#", $array["brief-name"], $raw );
        $array = json_decode( $raw, true );
        return $array;
    }
    
    public static function LoadResume()
    {
        self::$resume = self::ReadJson( self::$dataPath . "resume.json" );
        return self::$resume;
    }
    
    public static function LoadPortfolio()
    {
        self::$portfolio = self::ReadJson( self::$dataPath . "portfolio.json" );
        return self::$portfolio;
    }
};
?>
