<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="Rachel Wil Sha Singh">
    <link rel="icon" href="content/images/favicon.png">

<!-- Why hello there :)
<?
include_once( "backend.php" );
$resume = Storage::LoadResume();
?>
-->

    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <title> <?=$resume["name"] ?> - Résumé </title>

    <script src="content/jquery/jquery-3.3.1.min.js"></script>
    <link href="content/themes/2019-01/style.css" rel="stylesheet">
</head>



<body>
    <section class="content-holder cf">
        <div class="content-sidebar">
            
            <div class="my-info">
                <h1>Rachel Wil Sha Singh</h1>
                <p class="pronouns">(they/them)</p>
                <p class="image-logo"><img src="content/images/rach-face.png" title="Illustration of Rachel"></p>
                <p><span class="contact-info">
                    <?=$resume["phone"]?> <br>
                    <?=$resume["email"] ?>
                </span></p>
                <p><span class="available-location">
                    <?=$resume["availability"]?>
                </span></p>
            </div>

            <nav>

                <p class="header">Pages</p>
                <ul>
                    <li><a href="index.php">Résumé</a></li>
                    <li><a href="portfolio.php">Portfolio of Work</a></li>
                </ul>
                
                <p class="header">Résumé Navigation</p>
                <ul class="quick-access">
                    <li><a href="#summary">Summary</a></li>
                    <li><a href="#skills">Skills Overview</a></li>
                    <li><a href="#work">Work Experience</a></li>
                    <li><a href="#education">Education</a></li>
                    <li><a href="#volunteering">Volunteering</a></li>
                </ul>
                
                <p class="header">Projects</p>
                <ul class="projects">
                    <li><a href="http://bitbucket.org/rejcx">Rachel's BitBucket repositories</a></li>
                    <li><a href="http://www.moosader.com/">The Moosader homepage</a></li>
                    <li><a href="https://play.google.com/store/apps/developer?id=Moosader+LLC">Moosader on Google Play</a></li>
                    <li><a href="https://moosader.itch.io">Moosader on itch.io</a></li>
                    <li><a href="http://ayadanconlangs.com/">Áya Dan conlang resource</a></li>
                </ul>
                
                <p class="header">Links</p>
                <ul class="print">
                    <li><a href="linkedin.com/in/racheljmorris">Rachel on LinkedIn</a></li>
                </ul>
                
                <p class="header">Save for Later</p>
                <ul class="print">
                    <li><a href="javascript:window.print()">Print the Résumé!</a></li>
                </ul>

            </nav>
        </div>

        <div class="content-body">
            <div class="content">
                
                <h1>Résumé</h1>

                <div class="my-info">
                    <h1>Rachel Wil Sha Singh</h1>
                    <span class="contact-info">
                        (913) 999-8271 <br>
                        Rachel@LikesPizza.com
                    </span>
                    <hr>
                </div>
                

                <a name="summary">&nbsp;</a>
                <div class="summary-summary">
                    <h2>Summary</h2>
                    
                    <?=$resume["summary"]?>
                </div>

                <a name="skills">&nbsp;</a>
                <div class="resume-summary">
                    <h2>Skills Overview</h2>

                    <? foreach ( $resume["skills"] as $type => $list ) { ?>
                        <p>
                            <strong class="header"><?=$type?>: </strong>
                            <?
                            foreach ( $list as $ks => $skill )
                            {
                                if ( $ks != 0 ) { echo( ", " ); }
                                echo( $skill );
                            }
                            ?>
                        </p>
                    <? } ?>
                </div>

                <a name="work">&nbsp;</a>
                <h2>Work Experience</h2>

                <? foreach( $resume["work-experience"] as $key => $exp ) { ?>
                    <? if ( $exp["hidden"] == "true" ) { continue; } ?>
                    <div class="resume-entry job">
                        <div class="cf">
                            <div class="job-location location">
                                <h3 class="job-location location"><?=$exp["company"]?></h3>
                            </div>
                            <p class="title"><?=$exp["job-title"]?></p>
                        </div>
                        <p class="dates"><?=$exp["date-range"]?></p>
                        <p class="description"><?=$exp["description"]?></p>

                        <? if ( sizeof( $exp["skills"] ) > 0 ) { ?>
                            <p class="skills"><strong class="header">Skills: </strong>
                            <?
                            foreach ( $exp["skills"] as $ks => $skill )
                            {
                                if ( $ks != 0 ) { echo( ", " ); }
                                echo( $skill );
                            }
                            ?>
                            </p>
                        <? } ?>

                        <? if ( sizeof ( $exp["tasks"] ) > 0 ) { ?>
                            <p class="tasks"><strong class="header">Tasks: </strong>
                            <?
                            foreach ( $exp["tasks"] as $kt => $task )
                            {
                                if ( $kt != 0 ) { echo( ", " ); }
                                echo( $task );
                            }
                            ?>
                            </p>
                        <? } ?>

                        <? if ( sizeof ( $exp["taught"] ) > 0 ) { ?>
                            <p class="tasks"><strong class="header">Taught: </strong>
                            <?
                            foreach ( $exp["taught"] as $kt => $taught )
                            {
                                if ( $kt != 0 ) { echo( ", " ); }
                                echo( $taught );
                            }
                            ?>
                            </p>
                        <? } ?>

                        <? if ( sizeof ( $exp["links"] ) > 0 ) { ?>
                            <p><strong class="header">Links</strong></p>
                            <ul class="links">
                            <?
                            foreach ( $exp["links"] as $kt => $link )
                            {
                                ?>
                                <li><img src="content/images/icon-link.png" class="icon"> <a href="<?=$link["url"]?>"><?=$link["title"] ?></a> <span class="url-preview">(<?=$link["url"]?>)</span></li>
                                <?
                            }
                            ?>
                            </ul>
                            <br>
                        <? } ?>
                    </div>
                <? } ?>


                <a name="education">&nbsp;</a>
                <h2>Education</h2>
                <? foreach ( $resume["education"] as $key => $edu ) { ?>
                    <? if ( $edu["hidden"] == "true" ) { continue; } ?>
                    <div class="resume-entry education">
                        <div class="cf">
                            <div class="school location">
                                <h3 class="school location"><?=$edu["school"]?></h3>
                            </div>
                            <p class="degree"><?=$edu["degree"]?></p>
                        </div>
                        <p class="dates"><?=$edu["date-range"]?></p>
                        <p class="honors"><strong class="header">Honors: </strong><?=$edu["honors"]?></p>
                    </div>
                <? }?>


                <a name="volunteering">&nbsp;</a>
                <h2>Volunteering</h2>
                
                <? foreach ( $resume["volunteering"] as $key => $vol ) { ?>
                    <? if ( $vol["hidden"] == "true" ) { continue; } ?>
                    <div class="volunteering">
                        <p><strong class="header"><?=$vol["title"]?></strong>, <?=$vol["date-range"]?>, <?=$vol["description"]?></p>
                    </div>
                <? }?>
                
            </div>
        </div>
    </section>
</body>
