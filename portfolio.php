<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="Rachel Wil Sha Singh">
    <link rel="icon" href="content/images/favicon.png">

<!-- Why hello there :)
<?
include_once( "backend.php" );
$resume = Storage::LoadResume();
$portfolio = Storage::LoadPortfolio();
?>
-->

    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <title> <?=$resume["name"] ?> - Portfolio </title>

    <script src="content/jquery/jquery-3.3.1.min.js"></script>
    <link href="content/themes/2019-01/style.css" rel="stylesheet">
</head>

<body>
    <section class="content-holder cf">
        <div class="content-sidebar">
            
            <div class="my-info">
                <h1>Rachel Wil Sha Singh</h1>
                <p class="pronouns">(they/them)</p>
                <p class="image-logo"><img src="content/images/rach-face.png" title="Illustration of Rachel"></p>
                <p><span class="contact-info">
                    <?=$resume["phone"]?> <br>
                    <?=$resume["email"] ?>
                </span></p>
                <p><span class="available-location">
                    <?=$resume["availability"]?>
                </span></p>
            </div>

            <nav>

                <p class="header">Pages</p>
                <ul>
                    <li><a href="index.php">Résumé</a></li>
                    <li><a href="portfolio.php">Portfolio of Work</a></li>
                </ul>

                
                <p class="header">Portfolio Navigation</p>
                <ul class="quick-access">
                    <? foreach( $portfolio as $key => $area ) { ?>
                        <li><a href="#<?=$area['link-tag']?>"><?=$key?></a></li>
                    <? } ?>
                </ul>
                
                <p class="header">Projects</p>
                <ul class="projects">
                    <li><a href="http://bitbucket.org/rejcx">Rachel's BitBucket repositories</a></li>
                    <li><a href="http://www.moosader.com/">The Moosader homepage</a></li>
                    <li><a href="https://play.google.com/store/apps/developer?id=Moosader+LLC">Moosader on Google Play</a></li>
                    <li><a href="https://moosader.itch.io">Moosader on itch.io</a></li>
                    <li><a href="http://ayadanconlangs.com/">Áya Dan conlang resource</a></li>
                </ul>
                
                <p class="header">Links</p>
                <ul class="print">
                    <li><a href="linkedin.com/in/racheljmorris">Rachel on LinkedIn</a></li>
                </ul>
                
                <p class="header">Save for Later</p>
                <ul class="print">
                    <li><a href="javascript:window.print()">Print the Résumé!</a></li>
                </ul>

            </nav>
        </div>

        <div class="content-body">
            <div class="content">

                <h1>Portfolio</h1>

                <div class="my-info">
                    <h1>Rachel Wil Sha Singh</h1>
                    <span class="contact-info">
                        (913) 999-8271 <br>
                        Rachel@LikesPizza.com
                    </span>
                    <hr>
                </div>
                
                <? foreach( $portfolio as $key => $area ) { ?>
                    <a name="<?=$area['link-tag']?>">&nbsp;</a>
                    <div class="portfolio portfolio-software">
                        <h2><?=$key?></h2>
                        <p><?=$area['summary']?></p>
                        
                        <h3>Notable Work</h3>
                        <? foreach( $area['projects'] as $pk => $project ) { ?>
                            <div class="project cf">
                                <div class="info">
                                    <p class=""><span class="title"><?=$project['title']?></span> &nbsp; <?=$project['year']?></p>
                                    <p class="company"><strong><?=$project['company']?></strong></p>
                                    <p class="description"><?=$project['description']?></p>
                                    <? if ( isset( $project['tools'] ) ) { ?>
                                    <p class="tools"><strong class="header">Tools used: </strong><?=$project['tools']?></p>
                                    <? } ?>
                                    <? if ( isset( $project['link'] ) ) { ?>
                                    <p class="link"><img src="content/images/icon-link.png" class="icon"> <a href="<?=$project['link']['url']?>"><?=$project['link']['title']?></a></p>                                    
                                    <? } ?>
                                </div>
                                <div class="gallery cf">
                                    <? if ( isset( $project['images'] ) ) { ?>
                                        <h4>Images</h4>
                                        <? foreach( $project['images'] as $ik => $image ) { ?>
                                            <div class="image"><a href="content/images/portfolio/<?=$image?>"><img src="content/images/portfolio/<?=$image?>"></a></div>
                                        <? } ?>
                                    <? } ?>
                                </div>
                            </div>
                        <? } ?>
                    </div>
                <? } ?>

                


                
            </div>
        </div>
    </section>
</body>
